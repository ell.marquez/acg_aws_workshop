# Lab 4: Billing and Pricing

**under development** 

* Organizations 
* TCO calculator 
* Simple Pricing Calculator
* AWS Budgets
* AWS Support Plans and Trusted Advisor 

## Practice Tests 

[Quiz 1: Billing and Pricing ](https://www.surveymonkey.com/r/PDZ3Z7T) (uses Survey Monkey)

Done studying? Try the AWS practice sample questions;

[Quiz 2: AWS Practice Test](https://d1.awsstatic.com/training-and-certification/docs-cloud-practitioner/AWS-Certified-Cloud-Practioner_Sample_Questions_v1.1_FINAL.PDF)
