# **Lab 2: Cloud Concepts** 

* Goal:  Setting Up an Application Load Balancer with an Auto Scaling Group and Route 53 in AWS

## Part 1: Create and Configure an Application Load Balancer

**Notes:**  Make sure you're in the N. Virginia (us-east-1) region throughout the lab. 

1) Create and configure an Application Load Balancer.
2) Click on all services and choose EC2.
3) On the left side of your screen, you will see a scrolling menu. Under Load Balancing, choose Load Balancer.
4) Click on the blue button that reads Create Load Balancer.
5) Create an Application Load Balancer. 
6) Choose name. (Any name you want) 
    * Leave defaults below. 
7) Select only HTTP and select us-east-1a and us-east-1b availability zones as this is where our web servers will be located. 
    * Ignore the warning about using https as this server will not be used past this lab.
8) Click next and pick the existing security group.
9) Click next and choose a name for the target group.
    * Leave defaults for Target Group and Default.
    * Under Advance Health Check Settings, we are going to lower defaults to cut down our time on this lab. (Click on  for additional information on each of these options) 

    `Healthy threshold 3`

    `Unhealthy threshold 2`

    `Timeout  5 seconds`

    `Interval  10 seconds`

    `Success codes 200`   *ALS is looking for a 200 status code to pass the health checks.* 

    * Do not add any targets as these will be configured later. 
10) Click close and rejoice as you have successfully created your load balancer, providing fault tolerance in your environment. 

### Additional Resource: 

[ACG: What is Cloud Computing?](https://acloud.guru/course/aws-certified-cloud-practitioner/learn/Cloud-Concepts-And-Technology/what-is-cloud-computing/watch)

[AWS White Paper: What is Cloud Computing](https://docs.aws.amazon.com/whitepapers/latest/aws-overview/what-is-cloud-computing.html)


