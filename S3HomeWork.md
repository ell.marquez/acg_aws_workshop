# S3 Homework Lab

1) Go to S3 under the storage section. \
**Note:** that the region goes to Global. 

2) Click on `Create Bucket.`

3) Pick a bucket name. Remember, this needs to be a unique name and can 
only have alphanumeric characters.

4) Pick a region for your first bucket to be created. This will be 
replicated through others through AWS automation.

5) Block public access will make sure the bucket is only available to you. As we are working on learning concepts, go ahead, and uncheck this box.\
	**Remember** Delete these resources when you are done. 

6) Click to `Create The Bucket.`

7) Click on `Upload` and upload a random file. 
	Remember this file will be public, so might I suggest a funny meme?

8) If you click on the link, it will give you an access denied that because the file is not public. 

9) Go to `Actions` and click `Make Public`, and you will now be able to access the file. 

10) Look around the `Properties`, `Permissions`, and `Management` tabs.

11) Under properties review `Storage Classes` as you will need to know these for the test. 

12) Play around versioning under `Select From` section. 