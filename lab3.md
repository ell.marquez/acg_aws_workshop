# Lab 3 : Security and Compliance 

Lab 3 will be an educational walkthrough. Please follow along so that you can continue with the hands-on experience. 

## IAM  

1)  Activate MFA on your root account
    * Options: 
        * Google Authenticator (Available Anroid or IOS)
        * [Authy](https://authy.com/)
        * [Duo](https://duo.com/product/multi-factor-authentication-mfa/two-factor-authentication-2fa)
        * [YubiKey](https://www.yubico.com/)

2) Create individual IAM users
3) Use groups to assign permissions
4) Apply an IAM password policy
5) Rotate your access keys


### Additional Resources

[Step by Step Video Instructions](https://acloud.guru/course/aws-certified-cloud-practitioner/learn/Cloud-Concepts-And-Technology/lets-start-to-cloud-identity-access-management-iam/watch)

[AWS: MFA](https://aws.amazon.com/iam/features/mfa/?audit=2019q1)

[AWS: IAM User Guide](https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies.html)