# Additional Resources 

## General Resources
[AWS Certified Cloud Practitioner](https://aws.amazon.com/certification/certified-cloud-practitioner/)

[ACG AWS Certified Cloud Practitioner](https://acloud.guru/learn/aws-certified-cloud-practitioner)

[AWS Path to Training](https://aws.amazon.com/training/path-cloudpractitioner/)

[Take AWC CCP at home](https://aws.amazon.com/blogs/apn/now-you-can-take-the-aws-certified-cloud-practitioner-exam-at-your-home-or-office-24-7/)

[AWS Cloud Practitioner Essentials Cirriculum](https://www.aws.training/Details/Curriculum?id=27076)

## Lab 1

[AWS Free Tier](https://aws.amazon.com/free/)

## Lab 2

[ACG: What is Cloud Computing?](https://acloud.guru/course/aws-certified-cloud-practitioner/learn/Cloud-Concepts-And-Technology/what-is-cloud-computing/watch)

[AWS White Paper: What is Cloud Computing](https://docs.aws.amazon.com/whitepapers/latest/aws-overview/what-is-cloud-computing.html)


## Lab 2a 

 [ACG: Auto Scaling Video](https://acloud.guru/course/aws-certified-cloud-practitioner/learn/Cloud-Concepts-And-Technology/autoscaling/watch) 

[AWS: Auto Scaling Product Page](https://aws.amazon.com/ec2/autoscaling/)

## Lab 3 

[ACG: Step by Step Video Instructions](https://acloud.guru/course/aws-certified-cloud-practitioner/learn/Cloud-Concepts-And-Technology/lets-start-to-cloud-identity-access-management-iam/watch)

[AWS: MFA](https://aws.amazon.com/iam/features/mfa/?audit=2019q1)

[AWS: IAM User Guide](https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies.html)

### Additional White Papers 

[AWS Documentation](https://docs.aws.amazon.com/)

[Overview of Amazon Web Services whitepaper](https://d0.awsstatic.com/whitepapers/aws-overview.pdf) <-- **NOTE:** Review this thoroughly!

Architecting for the Cloud: AWS Best Practices whitepaper 

[How AWS Pricing Works whitepaper](https://d0.awsstatic.com/whitepapers/aws_pricing_overview.pdf)

[The Total Cost of (Non) Ownership of Web Applications in the Cloud whitepaper](https://media.amazonwebservices.com/AWS_TCO_Web_Applications.pdf)


## Ready to take the exam?

[Schedule](https://www.aws.training/certification?src=practitioner)